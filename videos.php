<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Jekyll v3.8.5">
        <title>Osupa Productions</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="css/visual.css" rel="stylesheet" type="text/css"/>

    </head>    

    <body style="background-image: url(img/backgrund_bege.jpg)">
        <!-- Button trigger modal -->

        <nav class="site-header  py-0" style="margin-top: 0px;">
            <div id="topo"> 
                <nav id="menu" class="topo2  navbar-expand-xl   " >

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        <div class="teko"><a href="index.php"><img src="img/logo-horinzotal.png" id="teste" alt="Osupa Productions" style="max-height: 70px; padding-top: 25px; padding-bottom: 5px;"></a> </div>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="top: 10px;
                         position: relative;">
                        <ul class="navbar-nav mr-auto" style="align-items: inherit;">

                            <?php
                            if (!isset($_COOKIE['resolucao'])) {
                                ?>
                                <script language='javascript'>
                                    document.cookie = "resolucao=" + screen.width + "x" + screen.height;
                                    self.location.reload();
                                </script>
                                <?php
                            } else {

                                $resolucao = list($width, $height) = explode("x", $_COOKIE['resolucao']);
//echo "<h3>Sua resolu&ccedil;&atilde;o &eacute; $width por $height</h3>";
                                if ($width >= 1024) {
                                    ?>
                                    <li class="nav-item" style="padding-right: 15px; padding-left: 15px;"><a href="index.php"> <img src="img/logo-horinzotal.png" id="teste" alt="Osupa Productions" 
                                                                                                                                    style="max-height: 118px; padding-right: 15px; padding-left: 15px; padding-bottom: 5px;"></a></li>
                                        <?php
                                    } else {
                                        
                                    }
                                }
                                ?>

                        </ul>                    
                    </div>
                </nav>
        </nav>        
        <div>         

            <ul class="nav justify-content-center mt-3 mb-3 ">
                <li class="nav-item mr-2">
                    <a class="nav-link btn btn-success btn-lg"  href="index.php">Início</a>
                </li>
                <li class="nav-item mr-2">
                   <a  class="nav-link   btn btn-dark btn-lg"  href="galeria.php"  aria-selected="true">Imagens</a>
                </li>
                <li class="nav-item mr-2">
                    <a class="nav-link  btn btn-dark btn-lg disabled"  href="" aria-selected="false">Vídeos</a>
                </li>  
            </ul>

            <div class="col-md-10 p-lg-4 mx-auto">

                <div class="card-columns">
                    <div class="card text-white bg-dark mb-4"  >
                        <a data-toggle="modal" data-target="#exampleModalCenter1" href="#!"><img class="card-img-top" src="img/play-videos2.jpg" alt="Card image cap"></a>
                        <div class="card-body">
                            <h5 class="card-text">Campanha Onde dói</h5>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4"  >
                        <a data-toggle="modal" data-target="#exampleModalCenter" href="#!"><img class="card-img-top" src="img/play-videos.png" alt="Card image cap"></a>
                        <div class="card-body">
                            <h5 class="card-text">O Visitante (Trailer)</h5>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#exampleModalCenter2" href="#!"><img class="card-img-top" src="img/play-videos.png" alt="Card image cap"></a>
                        <div class="card-body">
                            <h5 class="card-text">Reverb Voyeur (Trailer)</h5>
                        </div>                    
                    </div> 
                </div>

            </div>
            
            <!--             Modal 01 -->
            <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content modal-lg bg-dark">
                        <div class="modal-header ">                            
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe title="vimeo-player" src="https://player.vimeo.com/video/382937557" style="background-color: black;" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--             Modal 02 -->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content modal-lg bg-dark">
                        <div class="modal-header ">                            
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe title="vimeo-player" src="https://player.vimeo.com/video/292149666" style="background-color: black;" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--             Modal 03 -->
            <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content modal-lg bg-dark">
                        <div class="modal-header">                            
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe title="vimeo-player" src="https://player.vimeo.com/video/292149192" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>






        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>

</html>