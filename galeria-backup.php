<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Jekyll v3.8.5">
        <title>Osupa Productions</title>
        <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/product/">
        <!-- Bootstrap core CSS -->
        <link href="/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="icon" href="favicon.ico">
        <link href="css/visual.css" rel="stylesheet" type="text/css"/>
        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>        

        <!-- Custom styles for this template -->
        <link href="product.css" rel="stylesheet">

        <script src = "dist / sweetalert.min.js" ></script>
        <link rel = "stylesheet" type = "texto / css" href = "dist / sweetalert.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" integrity="sha256-iXUYfkbVl5itd4bAkFH5mjMEN5ld9t3OHvXX3IU8UxU=" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" integrity="sha256-egVvxkq6UBCQyKzRBrDHu8miZ5FOaVrjSqQqauKglKc=" crossorigin="anonymous"></script>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="css/galeria.css" rel="stylesheet" type="text/css"/>
        <link href="css/visual.css" rel="stylesheet" type="text/css"/>
        <style>


            #myImg {
                border-radius: 5px;
                cursor: pointer;
                transition: 0.3s;
            }

            #myImg:hover {opacity: 0.7;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (image) */
            .modal-content {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
            }

            /* Caption of Modal Image */
            #caption {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
            }

            /* Add Animation */
            .modal-content, #caption {  
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {-webkit-transform:scale(0)} 
                to {-webkit-transform:scale(1)}
            }

            @keyframes zoom {
                from {transform:scale(0)} 
                to {transform:scale(1)}
            }

            /* The Close Button */
            .close {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

            .close:hover,
            .close:focus {
                color: #bbb;
                text-decoration: none;
                cursor: pointer;
            }

            /* 100% Image Width on Smaller Screens */
            @media only screen and (max-width: 700px){
                .modal-content {
                    width: 100%;
                }
            }
        </style>
    </head>


    <body style="background-image: url(img/backgrund_bege.jpg)">
        <nav  site-header py-0 style="margin-top: 0px;">
            <div id="topo"> 
                <nav id="menu" class="topo2 navbar  navbar-expand-xl navbar-light  " >

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        <div class="teko"><a href="index.php"><img src="img/logo-horinzotal.png" id="teste" alt="Osupa Productions" style="max-height: 70px; padding-top: 25px; padding-bottom: 5px;"></a> </div>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="top: 10px;
                         position: relative;">
                        <ul class="navbar-nav mr-auto" style="align-items: inherit;">
                            <?php
                            if (!isset($_COOKIE['resolucao'])) {
                                ?>
                                <script language='javascript'>
                                    document.cookie = "resolucao=" + screen.width + "x" + screen.height;
                                    self.location.reload();
                                </script>
                                <?php
                            } else {

                                $resolucao = list($width, $height) = explode("x", $_COOKIE['resolucao']);
//echo "<h3>Sua resolu&ccedil;&atilde;o &eacute; $width por $height</h3>";
                                if ($width >= 1024) {
                                    ?>
                                    <li class="nav-item" style="padding-right: 15px; padding-left: 15px;"><a href="index.php"> <img src="img/logo-horinzotal.png" id="teste" alt="Osupa Productions" 
                                                                                                                                    style="max-height: 118px; padding-right: 15px; padding-left: 15px; padding-bottom: 5px;"></a></li>
                                        <?php
                                    } else {
                                        
                                    }
                                }
                                ?>
                        </ul>                    
                    </div>
                </nav>
        </nav>
        <div>
            <!-- The Modal -->
            <div id="myModal" class="modal">
                <span class="close">&times;</span>
                <img class="modal-content" id="img01">
                <div id="caption"></div>
            </div>          
            
                <ul class="nav justify-content-center mt-3 mb-3 ">
                <li class="nav-item mr-2">
                    <a class="nav-link active btn  btn-lg"  href="index.php" style="color:black;">Voltar</a>
                </li>
                <li class="nav-item mr-2">
                   <a  class="nav-link   btn btn-dark btn-lg disabled"  href=""  aria-selected="false">Imagens</a>
                </li>
                <li class="nav-item mr-2">
                    <a class="nav-link  btn btn-dark btn-lg "  href="videos.php" aria-selected="true">Vídeos</a>
                </li>  
            </ul>


                <div class="container">
                    <div class="tab-content" id="pills-tabContent">                        
                            <div class="Portfolio " onclick="img('img1')"><a href="#!"><img class="card-img" id="img1" src="img/galeria/galeria1.jpg" alt=""></a><div class="desc">Cactus in the Arctic Mirror sculpture</div></div>
                            <div class="Portfolio " onclick="img('img2')"><a href="#!"><img class="card-img" id="img2" src="img/galeria/galeria2.jpg" alt=""></a><div class="desc">Cactus in the Arctic Narsarsuaq Greenland</div></div>
                            <div class="Portfolio " onclick="img('img3')"><a href="#!"><img class="card-img" id="img3" src="img/galeria/galeria3.jpg" alt=""></a><div class="desc">Sculpture Mirror in the lake Amazon Forest</div></div> 
                            <div class="Portfolio " onclick="img('img4')"><a href="#!"><img class="card-img" id="img4" src="img/galeria/galeria4.jpg" alt=""></a><div class="desc">Tupiland Goes to Greenland</div></div>
                            <div class="Portfolio " onclick="img('img5')"><a href="#!"><img class="card-img" id="img5" src="img/galeria/galeria5.jpg" alt=""></a><div class="desc">Grupo Marzipan</div></div>
                            <div class="Portfolio " onclick="img('img6')"><a href="#!"><img class="card-img" id="img6" src="img/galeria/galeria6.jpg" alt=""></a><div class="desc">Performance the Great Arctic Mother and brazilian</div></div>
                            <div class="Portfolio " onclick="img('img7')"><a href="#!"><img class="card-img" id="img7" src="img/galeria/galeria7.jpg" alt=""></a><div class="desc">Sculpture bad hands in Amazon Forest</div></div>
                            <div class="Portfolio " onclick="img('img8')"><a href="#!"><img class="card-img" id="img8" src="img/galeria/galeria8.jpg" alt=""></a><div class="desc">Gabriel Mattos</div></div>
                            <div class="Portfolio " onclick="img('img11')"><a href="#!"><img class="card-img" id="img11" src="img/galeria/galeria11.jpg" alt=""></a><div class="desc">Gabriel Mattos Campeão de Judô </div></div>
                            <div class="Portfolio " onclick="img('img12')"><a href="#!"><img class="card-img" id="img12" src="img/galeria/galeria12.jpg" alt=""></a><div class="desc">Gabriel Mattos - Medalha de Ouro</div></div>
                            <div class="Portfolio " onclick="img('img13')"><a href="#!"><img class="card-img" id="img13" src="img/galeria/galeria13.jpg" alt=""></a><div class="desc">Circuito Baiano de Judô</div></div>
                            <div class="Portfolio " onclick="img('img14')"><a href="#!"><img class="card-img" id="img14" src="img/galeria/galeria14.jpg" alt=""></a><div class="desc">Gabriel Mattos</div></div>    
                    </div>
                </div>
            </div>


        </div>



    <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script></body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
                                // Get the modal
                                var modal = document.getElementById("myModal");

                                // Get the image and insert it inside the modal - use its "alt" text as a caption
                                function img(imagem) {
                                    var img = document.getElementById(imagem);
                                    var modalImg = document.getElementById("img01");
                                    var captionText = document.getElementById("caption");
                                    img.onclick = function () {
                                        modal.style.display = "block";
                                        modalImg.src = this.src;
                                        captionText.innerHTML = this.alt;
                                    }
                                }

                                // Get the <span> element that closes the modal
                                var span = document.getElementsByClassName("close")[0];

                                // When the user clicks on <span> (x), close the modal
                                span.onclick = function () {
                                    modal.style.display = "none";
                                }
    </script>

</body>

</html>