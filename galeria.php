<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Jekyll v3.8.5">
        <title>Osupa Productions</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="css/visual.css" rel="stylesheet" type="text/css"/>

    </head>    

    <body style="background-image: url(img/backgrund_azul.jpg)">
        <!-- Button trigger modal -->

        <nav class="site-header  py-0" style="margin-top: 0px;">
            <div id="topo"> 
                <nav id="menu" class="topo2  navbar-expand-xl   " >

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        <div class="teko"><a href="index.php"><img src="img/logo-horinzotal.png" id="teste" alt="Osupa Productions" style="max-height: 70px; padding-top: 25px; padding-bottom: 5px;"></a> </div>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="top: 10px;
                         position: relative;">
                        <ul class="navbar-nav mr-auto" style="align-items: inherit;">

                            <?php
                            if (!isset($_COOKIE['resolucao'])) {
                                ?>
                                <script language='javascript'>
                                    document.cookie = "resolucao=" + screen.width + "x" + screen.height;
                                    self.location.reload();
                                </script>
                                <?php
                            } else {

                                $resolucao = list($width, $height) = explode("x", $_COOKIE['resolucao']);
//echo "<h3>Sua resolu&ccedil;&atilde;o &eacute; $width por $height</h3>";
                                if ($width >= 1024) {
                                    ?>
                                    <li class="nav-item" style="padding-right: 15px; padding-left: 15px;"><a href="index.php"> <img src="img/logo-horinzotal.png" id="teste" alt="Osupa Productions" 
                                                                                                                                    style="max-height: 118px; padding-right: 15px; padding-left: 15px; padding-bottom: 5px;"></a></li>
                                        <?php
                                    } else {
                                        
                                    }
                                }
                                ?>

                        </ul>                    
                    </div>
                </nav>
        </nav>        
        <div>         

            <ul class="nav justify-content-center mt-3 mb-3 ">
                <li class="nav-item mr-2">
                    <a class="nav-link btn btn-success btn-lg"  href="index.php">Início</a>
                </li>
                <li class="nav-item mr-2">
                    <a  class="nav-link btn btn-dark btn-lg disabled"  href="galeria.php"  aria-selected="true">Imagens</a>
                </li>
                <li class="nav-item mr-2">
                    <a class="nav-link btn btn-dark btn-lg"  href="videos.php" aria-selected="false">Vídeos</a>
                </li>  
            </ul>


            <div class="col-md-10 p-lg-4 mx-auto">

                <div class="card-columns">
                    <div class="card text-white bg-dark mb-4"  >
                        <a data-toggle="modal" data-target="#imagem1" href="#!"><img class="card-img" src="img/galeria/galeria1.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Tupiland Goes to Greenland</p>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem2" href="#!"><img class="card-img" src="img/galeria/galeria2.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Ensaio fotográfico</p>
                        </div>                    
                    </div> 
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem3" href="#!"><img class="card-img" src="img/galeria/galeria3.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text"></p>
                        </div>                    
                    </div> 
                    <div class="card text-white bg-dark mb-4"  >
                        <a data-toggle="modal" data-target="#imagem4" href="#!"><img class="card-img" src="img/galeria/galeria12.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Gabriel Mattos - Medalha de Ouro</p>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem5" href="#!"><img class="card-img" src="img/galeria/galeria5.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Grupo Marzipan</p>
                        </div>                    
                    </div> 
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem6" href="#!"><img class="card-img" src="img/galeria/galeria6.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Hands on produção na Amazônia</p>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4"  >
                        <a data-toggle="modal" data-target="#imagem7" href="#!"><img class="card-img" src="img/galeria/galeria7.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text"></p>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem8" href="#!"><img class="card-img" src="img/galeria/galeria8.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Gabriel Mattos</p>
                        </div>                    
                    </div> 
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem9" href="#!"><img class="card-img" src="img/galeria/galeria11.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Ensaio Fotográfico</p>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4"  >
                        <a data-toggle="modal" data-target="#imagem10" href="#!"><img class="card-img" src="img/galeria/galeria4.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Tupiland Goes to Greenland</p>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem11" href="#!"><img class="card-img" src="img/galeria/galeria13.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Série Irmãos Freitas</p>
                        </div>                    
                    </div> 
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem12" href="#!"><img class="card-img" src="img/galeria/galeria14.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">produção ensaio fotográfico</p>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem15" href="#!"><img class="card-img" src="img/galeria/galeria15.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Peça de teatro E se fosse você?</p>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem16" href="#!"><img class="card-img" src="img/galeria/galeria16.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text"></p>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem17" href="#!"><img class="card-img" src="img/galeria/galeria17.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Campanha Onde Dói</p>
                        </div>                    
                    </div>
                    <div class="card text-white bg-dark mb-4" >
                        <a data-toggle="modal" data-target="#imagem18" href="#!"><img class="card-img" src="img/galeria/galeria18.jpg" alt=""></a>
                        <div class="card-body">
                            <p class="card-text">Produção de lançamento do livro em Salvador</p>
                        </div>                    
                    </div>
                </div>

            </div>

            <!--             Modal 01 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem1" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria1.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 02 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem2" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria2.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 03 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem3" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria3.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 04 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem4" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria12.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 05 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem5" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria5.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 06 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem6" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria6.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 07 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem7" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria7.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 08 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem8" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria8.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 09 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem9" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria11.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 10 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem10" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria4.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 11 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem11" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria13.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 12 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem12" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria14.jpg" alt="">
                    </div>
                </div>
            </div>

            <!--             Modal 13 -->  
            <div class="modal fade bd-example-modal-xl" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria1.jpg" alt="">
                    </div>
                </div>
            </div>
            
              <!--             Modal 15 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem15" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria15.jpg" alt="">
                    </div>
                </div>
            </div>
              
                <!--             Modal 16 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem16" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria16.jpg" alt="">
                    </div>
                </div>
            </div>
                
                  <!--             Modal 17 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem17" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria17.jpg" alt="">
                    </div>
                </div>
            </div>
                    <!--             Modal 18 -->  
            <div class="modal fade bd-example-modal-xl" id="imagem18" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-header">                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-content">
                        <img class="card-img"  src="img/galeria/galeria18.jpg" alt="">
                    </div>
                </div>
            </div>








            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>

</html>