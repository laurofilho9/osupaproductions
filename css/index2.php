
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Jekyll v3.8.5">
        <title>Product example · Bootstrap</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/product/">

        <!-- Bootstrap core CSS -->
        <link href="/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link href="css/visual.css" rel="stylesheet" type="text/css"/>


        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>        

        <!-- Custom styles for this template -->
        <link href="product.css" rel="stylesheet">

        <script src = "dist / sweetalert.min.js" ></script>
        <link rel = "stylesheet" type = "texto / css" href = "dist / sweetalert.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" integrity="sha256-iXUYfkbVl5itd4bAkFH5mjMEN5ld9t3OHvXX3IU8UxU=" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" integrity="sha256-egVvxkq6UBCQyKzRBrDHu8miZ5FOaVrjSqQqauKglKc=" crossorigin="anonymous"></script>

    </head>
    <body>        
        <nav class="site-header sticky-top py-0" style="margin-top: 0px;">
            <div id="topo"> 
                <nav id="menu" class="topo2 navbar  navbar-expand-xl navbar-light  " >

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        <div class="teko"><a href="index.php"><img src="img/logo-horinzotal.png" id="teste" alt="Osupa Production" style="max-height: 70px; padding-top: 25px; padding-bottom: 5px;"></a> </div>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="top: 10px;
                         position: relative;">
                        <ul class="navbar-nav mr-auto" style="align-items: inherit;">
                            <li class="nav-item active" style="padding: 5px;">
                                <a href="#pagQuemSomos"> <button type="button" class="btn btn-outline-light btn-lg" style="width: 160px; padding: 5px;">Quem Somos</button></a>
                            </li>
                            <li class="nav-item" style="padding: 5px;">
                                <a href="#pagEventos"> <button type="button" class="btn btn-outline-light btn-lg" style="width: 160px; padding: 5px;">Eventos</button></a>
                            </li>



                            <?php
                            if (!isset($_COOKIE['resolucao'])) {
                                ?>
                                <script language='javascript'>
                                    document.cookie = "resolucao=" + screen.width + "x" + screen.height;
                                    self.location.reload();
                                </script>
                                <?php
                            } else {

                                $resolucao = list($width, $height) = explode("x", $_COOKIE['resolucao']);
//echo "<h3>Sua resolu&ccedil;&atilde;o &eacute; $width por $height</h3>";
                                if ($width >= 1024) {
                                    ?>
                                    <li class="nav-item" style="padding-right: 15px; padding-left: 15px;"><a href="index.php"> <img src="img/logo-horinzotal.png" id="teste" alt="Osupa Production" 
                                                                                                                                    style="max-height: 118px; padding-right: 15px; padding-left: 15px; padding-bottom: 5px;"></a></li>
                                        <?php
                                    } else {
                                        
                                    }
                                }
                                ?>





                            <li class="nav-item active" style="padding: 5px;">
                                <a href="#pagContato"> <button type="button" class="btn btn-outline-light btn-lg" style="width: 160px; padding: 5px;">Contato</button></a>
                            </li>
                            <li class="nav-item active" style="padding: 5px;">
                                <button type="button" class="btn btn-outline-light btn-lg" style="width: 160px; padding: 5px;">Login</button>
                            </li>
                        </ul>                    
                    </div>
                </nav>
        </nav>

        <!--        Página Inicial-->
        <div class="corpo" > 
            <!--                Pré Banner-->
            <div class="col-md-8 p-lg-4 mx-auto">                
                <div id="centro" class="container">
                    <div class="mx-auto my-auto pre-banner">
                        <div class="texto-pre-banner">
                            <h3></h3>
                        </div>
                    </div>
                </div>

                <!--        Banner Rotativo-->   
                <div class="banner-rotativo border border-white rounded">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="img/banner1.jpg" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Filme: Nome</h5>
                                    <p>Gravação do filme na cidade de Salvador-Ba</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="img/banner2.jpg" class="d-block w-100" alt="...">
                                <!--                            <div class="carousel-caption d-none d-md-block">
                                                                <h5>First slide label</h5>
                                                                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                                            </div>-->
                            </div>
                            <div class="carousel-item">
                                <img src="img/banner3.jpg" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Musical: Nome</h5>
                                    <p>Musical apresentado em...</p>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div> 
    </div>

    <!--    PÁGINA QUEM SOMOS-->
    <section id="pagQuemSomos">      
        <div class="quem-somos">
            <div class="col-md-10 p-lg-4 mx-auto">      
                <div class="titulo-paginas texto-quemsomos">            
                    <h1>Quem Somos</h1>            
                </div>
                <div class="separador">                    
                    <img src="img/separador.png" alt="" style="width: 20%" />                    
                </div>
                <div class="texto-pagina"> 
                    <p>A <span class="osupa"negrito>Osupa Production </span> é uma Produtora Cultural com sede no Brasil e nos Estados Unidos. Atua Desenvolvendo e ou apoiando projetos culturais, sociais, de meio ambiente e esportivos.
                        Nossa missão e disseminar a cultura nas suas mais diversas variações artísticas como;
                        Teatro, dança, áudio-visual, literatura e artes plasticas. Apoiando e realizando projetos sócio-culturais e esportivos em parcerias públicas ou privadas, Tendo como premissa responsabilidade cultural, social, e ambiental.
                        Valorizamos a ética, criatividade, diversidade, inovação, coletividade e sustentabilidade.
                        O objetivo da <span class="osupa"negrito>Osupa </span> é ser referência em produção cultural e entretenimento no Brasil e no Exterior.
                    </p>
                </div>

                <div class="row">
                    <div class="col-lg-3 centralizar">
                        <img class="img-circle" src="img/circ-culturall.png" alt="" width="160" height="160">
                        <h3 class="">Cultural</h3>
<!--                        <p class="justificar">Texto falando sobre a importância da Osupa Production nos projetos culturais no Brasil e EUA. Texto falando sobre a importância da Osupa Production nos projetos culturais no Brasil e EUA.</p> -->
<!--                        <p><a class="btn btn-default" href="#" role="button">Ver detalhes »</a></p>-->
                    </div>
                    <div class="col-lg-3 centralizar">
                        <img class="img-circle" src="img/circ-social.png" alt="" width="160" height="160">
                        <h3 class="">Social</h3>
<!--                        <p class="justificar">Texto falando sobre a importância da Osupa Production nos projetos sociais no Brasil e EUA. Texto falando sobre a importância da Osupa Production nos projetos culturais no Brasil e EUA.</p> -->
<!--                        <p><a class="btn btn-default" href="#" role="button">Ver detalhes »</a></p>-->
                    </div>
                    <div class="col-lg-3 centralizar">
                        <img class="img-circle" src="img/circ-meioambiente.png" alt="" width="160" height="160">
                        <h3 class="">Ambiental</h3>
<!--                        <p class="justificar">Texto falando sobre a importância da Osupa Production nos projetos ambientais no Brasil e EUA. Texto falando sobre a importância da Osupa Production nos projetos culturais no Brasil e EUA.</p> -->
<!--                        <p><a class="btn btn-default" href="#" role="button">Ver detalhes »</a></p>-->
                    </div>
                    <div class="col-lg-3 centralizar">
                        <img class="img-circle" src="img/circ-esporte.png" alt="" width="160" height="160">
                        <h3 class="">Esportivo</h3>
<!--                        <p class="justificar">Texto falando sobre a importância da Osupa Production nos projetos esportivos no Brasil e EUA. Texto falando sobre a importância da Osupa Production nos projetos culturais no Brasil e EUA.</p> -->
<!--                        <p><a class="btn btn-default" href="#" role="button">Ver detalhes »</a></p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--    PÁGINA EVENTOS-->
<section id="pagEventos"> 
    <div class="eventos">
        <div class="col-md-10 p-lg-4 mx-auto">      
            <div class="titulo-paginas texto-quemsomos">            
                <h1>Eventos</h1>            
            </div>
            <div class="separador">                    
                <img src="img/separador.png" alt="" style="width: 20%" />                    
            </div>

            <section>
                <div class="container py-1">                    
                    <div class="row">
                        <div class="col-md-3 alignnone">
                            <img src="img/05.jpg" class="w-100">
                        </div> 
                        <div class="col-md-8 px-3">
                            <div class="card-block px-2 py-2">
                                <h4 class="card-title" style="font-weight: bold;">CERIMÔNIA DE ABERTURA MIMB - 2019</h4>
                                <p class="card-text"><small class="text-muted">Atualizados 3 minutos atrás</small></p>
                                <p class="card-text texto-eventos"> Evento realizado pelo SESC Pelourinho, para cerimônia de abertura do MIMB, com Margaret Menezes como convidada especial. </p>
                                <a href="#pagEventos" onclick="Texto1();" class="card-link">Leia mais...</a>
                                <script type="text/javascript">
                                    function Texto1() {
                                        swal({
                                            title: "CERIMÔNIA DE ABERTURA MIMB - 2019",                                            
                                            text: 'A <span style="color:#7ABABA";> Osupa Production </span> teve a honra de patrocinar o evento MIMB - 2019. <br> A cerimônia de abertura da MIMB 2019 teve muitas surpresas. Aconteceu no dia 14 de agosto, além das exibições de filmes nacionais e internacionais, o evento teve a mistura potente e dançante da banda Afrocidade.',
                                                    html: true
                                        });
                                    }
                                </script>
                            </div>
                        </div>
                    </div>                    
                </div>        
            </section>

            <section>
                <div class="container py-3">                    
                    <div class="row">
                        <div class="col-md-3 alignnone">
                            <img src="img/06.jpg" class="w-100">
                        </div> 
                        <div class="col-md-8 px-3">
                            <div class="card-block px-2 py-2">
                                <h4 class="card-title" style="font-weight: bold;">FUTURO DIGITAL CONFERENCE</h4>
                                <p class="card-text"><small class="text-muted">Atualizados 3 minutos atrás</small></p>
                                <p class="card-text texto-eventos"> Festival de Cinema Latino-Americano de Nova York (NYLFF), o principal festival de cinema da cidade de Nova York, programas de competição e programas comunitários. </p>
                                <a href="#pagEventos" onclick="Texto2();" class="card-link">Leia mais...</a>
                                <script type="text/javascript">
                                    function Texto2() {
                                        swal({
                                            title: "FUTURO DIGITAL CONFERENCE!",                                            
                                            text: 'A Festival de Cinema Latino-Americano de Nova York (NYLFF), o principal festival de cinema da cidade de Nova York. Desde sua fundação em 1999, o NYLFF produz experiências culturalmente relevantes e divertidas que constroem platéias para o cinema latino, apoiam a comunidade cinematográfica com desenvolvimento profissional e fomentam relacionamentos para talentos latinos. A programação inclui o principal festival de cinema da cidade de Nova York, programas de competição e programas comunitários. ',
                                                   html: true
                                        });
                                    }
                                </script>
                            </div>
                        </div>
                    </div>                    
                </div>        
            </section>

            <section>
                <div class="container py-3">                    
                    <div class="row">
                        <div class="col-md-3 alignnone">
                            <img src="img/07.jpg" class="w-100">
                        </div> 
                        <div class="col-md-8 px-3">
                            <div class="card-block px-2 py-2">
                                <h4 class="card-title" style="font-weight: bold;">CIRCUITO BAIANO DE JUDÔ</h4>
                                <p class="card-text"><small class="text-muted">Atualizados 1 minutos atrás</small></p>
                                <p class="card-text texto-eventos"> Circuito Baiano de Judô vencido por nosso atleta Gabriel Matos. </p>
                                <a href="#pagEventos" onclick="Texto3();" class="card-link">Leia mais...</a>
                                <script type="text/javascript">
                                    function Texto3() {
                                        swal({
                                            title: "Gabriel Matos - Atleta de Judô",                                            
                                            text: 'A <span style="color:#7ABABA";> Osupa Production </span> parabeniza nosso atleta patrocinado Gabriel Matos pela medalha de <span style="color:#FFD700";> Ouro </span> conquistada no Circuito Baiano de Judô..',
                                                   html: true
                                        });
                                    }
                                </script>
                            </div>
                        </div>
                    </div>                    
                </div>        
            </section>

        </div>
    </div>
</div>
</section>

<!--        Pagína Contato-->
<section id="pagContato">        
    <div class="contato"> 
        <div class="col-md-10  p-lg-4 mx-auto">                
            <div class="container contact">                    
                <div class="row" style="margin-top: 3%">
                    <div class="col-md-5">                            
                        <div style="margin-top: 10%; text-align: center; color: white;">
                            <h2 style="margin-bottom: 5%">Siga-nos nas redes sociais.</h2>      
                            <a href="pagina.html" target="_blank" onmouseover="document.NOME.src = 'img/twitter.png'" onmouseout="document.NOME.src = 'img/twitter2.png'"><img src="img/twitter.png" name="NOME" Border="0"></a>                                
                            <a href="https://instagram.com/osupaproduction" target="_blank" onmouseover="document.NOME1.src = 'img/instagram.png'" onmouseout="document.NOME1.src = 'img/instagram2.png'"><img src="img/instagram.png" name="NOME1" Border="0"></a>
                            <a href="pagina.html" target="_blank" onmouseover="document.NOME2.src = 'img/youtube.png'" onmouseout="document.NOME2.src = 'img/youtube2.png'"><img src="img/youtube.png" name="NOME2" Border="0"></a>  <br>        

                        </div>
                        <!--                            </div>-->
                    </div>
                    <div class="col-md-7">
                        <div class="contact-form">				
                            <div class="form-group">
                                <div class="contact-info col-sm-10" style="color: #fff1ce;">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-2" style="padding-left: 0;">
                                                <img src="https://image.ibb.co/kUASdV/contact-image.png" alt="image"/>
                                            </div>
                                            <div class="col-10" style="text-align: center;">
                                                <h1>Entre em contato</h1>  
                                                <h4>Gostaríamos de ouvir você</h4> 
                                            </div>                                                
                                        </div>
                                    </div>                                        
                                </div>
                                <div class="col-sm-10" style="margin-top: 3%;">          
                                    <input type="text" class="form-control" id="lname" placeholder="Digite seu nome" name="lname">
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" placeholder="Digite seu email" name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" id="comment"></textarea>
                                </div>
                            </div>
                            <div class="form-group">        
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-light" style=" float:right">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>           
    </div> 
</section>   



<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script></body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
