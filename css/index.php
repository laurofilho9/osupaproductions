<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Jekyll v3.8.5">
        <title>Osupa Productions</title>
        <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/product/">
        <!-- Bootstrap core CSS -->
        <link href="/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="icon" href="favicon.ico">        
        <link href="css/visual.css" rel="stylesheet" type="text/css"/>
        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>        

        <!-- Custom styles for this template -->
        <link href="product.css" rel="stylesheet">

        <script src = "dist / sweetalert.min.js" ></script>
        <link rel = "stylesheet" type = "texto / css" href = "dist / sweetalert.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" integrity="sha256-iXUYfkbVl5itd4bAkFH5mjMEN5ld9t3OHvXX3IU8UxU=" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" integrity="sha256-egVvxkq6UBCQyKzRBrDHu8miZ5FOaVrjSqQqauKglKc=" crossorigin="anonymous"></script>

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="css/galeria.css" rel="stylesheet" type="text/css"/>
        <link href="css/visual.css" rel="stylesheet" type="text/css"/>
        <style>


            #myImg {
                border-radius: 5px;
                cursor: pointer;
                transition: 0.3s;
            }

            #myImg:hover {opacity: 0.7;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (image) */
            .modal-content {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
            }

            /* Caption of Modal Image */
            #caption {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
            }

            /* Add Animation */
            .modal-content, #caption {  
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {-webkit-transform:scale(0)} 
                to {-webkit-transform:scale(1)}
            }

            @keyframes zoom {
                from {transform:scale(0)} 
                to {transform:scale(1)}
            }

            /* The Close Button */
            .close {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

            .close:hover,
            .close:focus {
                color: #bbb;
                text-decoration: none;
                cursor: pointer;
            }

            /* 100% Image Width on Smaller Screens */
            @media only screen and (max-width: 700px){
                .modal-content {
                    width: 100%;
                }
            }
        </style>

    </head>
    <body>        
        <nav class="site-header sticky-top py-0" style="margin-top: 0px;">
            <div id="topo"> 
                <nav id="menu" class="topo2 navbar  navbar-expand-xl navbar-light  " >

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        <div class="teko"><a href="index.php"><img src="img/logo-horinzotal.png" id="teste" alt="Osupa Productions" style="max-height: 70px; padding-top: 25px; padding-bottom: 5px;"></a> </div>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="top: 10px;
                         position: relative;">
                        <ul class="navbar-nav mr-auto" style="align-items: inherit;">
                            <li class="nav-item active" style="padding: 5px;">
                                <a href="#pagQuemSomos" class="btn btn-link btn-lg" style="width: 160px; padding: 5px; text-decoration: none"> <span class="titulo-botoes"> Quem Somos </span></a>
                            </li>
                            <li class="nav-item" style="padding: 5px;">
                                <a href="#pagProjetos" class="btn btn-link btn-lg" style="width: 160px; padding: 5px; text-decoration: none"> <span class="titulo-botoes">Projetos</span></a>
                            </li>



                            <?php
                            if (!isset($_COOKIE['resolucao'])) {
                                ?>
                                <script language='javascript'>
                                    document.cookie = "resolucao=" + screen.width + "x" + screen.height;
                                    self.location.reload();
                                </script>
                                <?php
                            } else {

                                $resolucao = list($width, $height) = explode("x", $_COOKIE['resolucao']);
//echo "<h3>Sua resolu&ccedil;&atilde;o &eacute; $width por $height</h3>";
                                if ($width >= 1024) {
                                    ?>
                                    <li class="nav-item" style="padding-right: 15px; padding-left: 15px;"><a href="index.php"> <img src="img/logo-horinzotal.png" id="teste" alt="Osupa Productions" 
                                                                                                                                    style="max-height: 118px; padding-right: 15px; padding-left: 15px; padding-bottom: 5px;"></a></li>
                                        <?php
                                    } else {
                                        
                                    }
                                }
                                ?>

                            <li class="nav-item active" style="padding: 5px;">
                                <a href="galeria.php" class="btn btn-link btn-lg" style="width: 160px; padding: 5px; text-decoration: none"> <span class="titulo-botoes">Galeria </span></a>
                            </li>
                            <li class="nav-item active" style="padding: 5px;">
                                <a href="#pagContato" class="btn btn-link btn-lg" style="width: 160px; padding: 5px; text-decoration: none"> <span class="titulo-botoes">Contato </span></a>
                            </li>
                        </ul>                    
                    </div>
                </nav>
        </nav>

        <!--        Página Inicial-->
        <div class="corpo" > 
            <!--                Pré Banner-->
            <div class="col-md-8 p-lg-4 mx-auto">                
                <div id="centro" class="container">
                    <div class="mx-auto my-auto pre-banner">
                        <div class="texto-pre-banner">
                            <h3></h3>
                        </div>
                    </div>
                </div>

                <!--        Banner Rotativo-->   
                <div class="banner-rotativo border border-white rounded">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="img/banner1.jpg" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Filme: Nome</h5>
                                    <p>Gravação do filme na cidade de Salvador-Ba</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="img/banner5.jpg" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Grupo Marzipan</h5>
<!--                                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>-->
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="img/banner3.jpg" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Musical: Nome</h5>
                                    <p>Musical apresentado em...</p>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div> 
    </div>

    <!--    PÁGINA QUEM SOMOS-->
    <section id="pagQuemSomos">      
        <div class="quem-somos">
            <div class="col-md-10 p-lg-5 mx-auto">      
                <div class="titulo-paginas">            
                    <span>Quem Somos</span>            
                </div>
                <div class="separador">                    
                    <img src="img/separador.png" alt="" style="width: 20%" />                    
                </div>
                <div class="texto-pagina"> 
                    <p>A <span class="osupa"negrito>Osupa Productions </span> é uma Produtora Cultural com sede no Brasil e nos Estados Unidos. Atua Desenvolvendo e ou apoiando projetos culturais, sociais, de meio ambiente e esportivos.
                        Nossa missão e disseminar a cultura nas suas mais diversas variações artísticas como;
                        Teatro, dança, áudio-visual, literatura e artes plasticas. Apoiando e realizando projetos sócio-culturais e esportivos em parcerias públicas ou privadas, Tendo como premissa responsabilidade cultural, social, e ambiental.
                        Valorizamos a ética, criatividade, diversidade, inovação, coletividade e sustentabilidade.
                        O objetivo da <span class="osupa"negrito>Osupa </span> é ser referência em produção cultural e entretenimento no Brasil e no Exterior.
                    </p>
                </div>

                <div class="row">
                    <a href="#pagQuemSomos" onclick="TextoCultural();">
                        <div class="col-lg-3 centralizar">
                            <img class="img-circle" src="img/circ-culturall.png" alt="" width="160" height="160">
                            </a>
                            <script type="text/javascript">
                                function TextoCultural() {
                                    swal({
                                        title: "Projetos culturais",
                                        text: '<span style="color:#7ABABA";> Teatro: </span> • Crônicas em Cena, • No Mundo da Lua, • O Ônibus, • Prevenção, \n\
                                                            • Um Dedo de prosa, • O Casamento da chacrinha. <br> <br> <span style="color:#7ABABA";> Cinema: </span> \n\
                                                            • Irmãos Freitas, • Festival Itinerante de Cinema Negro da Bahia - MIMB, • O Visitante, • Reverb Voyeur.\n\
                                                            <br> <br> <span style="color:#7ABABA";> Dança: </span> • Espetáculo de dança Marzipan.\n\
                                                            <br> <br> <span style="color:#7ABABA";> Música: </span> • Dão Nobre Balanço. \n\
                                                            <br> <br> <span style="color:#7ABABA";> Artes Plásticas: </span> • Thiago Costackz. ',
                                        confirmButtonText: 'Voltar',
                                        confirmButtonColor: '#7ababa',
                                        html: true
                                    });
                                }
                            </script>
                            <h1 class="titulo-materia">Cultural</h1>
                            <p>
                        </div>

                        <a href="#pagQuemSomos" onclick="TextoSocial();">    
                            <div class="col-lg-3 centralizar">
                                <img class="img-circle" src="img/circ-social.png" alt="" width="160" height="160">
                                </a>
                                <script type="text/javascript">
                                    function TextoSocial() {
                                        swal({
                                            title: "Projetos Sociais apoiados / realizados pela Osupa Productions",
                                            text: '<span style="color:#7ABABA";> • Centro Espirita Renascer da Esperança : </span>',
                                            confirmButtonText: 'Voltar',
                                            confirmButtonColor: '#7ababa',
                                            html: true
                                        });
                                    }
                                </script>
                                <h1 class="titulo-materia">Social</h1>
        <!--                        <p class="justificar">Texto falando sobre a importância da Osupa Production nos projetos sociais no Brasil e EUA. Texto falando sobre a importância da Osupa Production nos projetos culturais no Brasil e EUA.</p> -->
        <!--                        <p><a class="btn btn-default" href="#" role="button">Ver detalhes »</a></p>-->
                            </div>

                            <a href="#pagQuemSomos" onclick="TextoAmbiental();">    
                                <div class="col-lg-3 centralizar">
                                    <img class="img-circle" src="img/circ-meioambiente.png" alt="" width="160" height="160">
                                    </a>    
                                    <script type="text/javascript">
                                        function TextoAmbiental() {
                                            swal({
                                                title: "Projetos Ambientais apoiados / realizados pela Osupa Productions",
                                                text: '<span style="color:#7ABABA";> • SOS Terra </span>',
                                                confirmButtonText: 'Voltar',
                                                confirmButtonColor: '#7ababa',
                                                html: true
                                            });
                                        }
                                    </script>
                                    <h1 class="titulo-materia">Ambiental</h1>
            <!--                        <p class="justificar">Texto falando sobre a importância da Osupa Production nos projetos ambientais no Brasil e EUA. Texto falando sobre a importância da Osupa Production nos projetos culturais no Brasil e EUA.</p> -->
            <!--                        <p><a class="btn btn-default" href="#" role="button">Ver detalhes »</a></p>-->
                                </div>

                                <a href="#pagQuemSomos" onclick="TextoEsportivo();">
                                    <div class="col-lg-3 centralizar">
                                        <img class="img-circle" src="img/circ-esporte.png" alt="" width="160" height="160">
                                        </a>
                                        <script type="text/javascript">
                                            function TextoEsportivo() {
                                                swal({
                                                    title: "Projetos Esportivos apoiados / realizados pela Osupa Productions",
                                                    text: '<span style="color:#7ABABA";> Judô: </span> Gabriel Mattos.',
                                                    confirmButtonText: 'Voltar',
                                                    confirmButtonColor: '#7ababa',
                                                    html: true
                                                });
                                            }
                                        </script>
                                        <h1 class="titulo-materia">Esportivo</h1>
                <!--                        <p class="justificar">Texto falando sobre a importância da Osupa Production nos projetos esportivos no Brasil e EUA. Texto falando sobre a importância da Osupa Production nos projetos culturais no Brasil e EUA.</p> -->
                <!--                        <p><a class="btn btn-default" href="#" role="button">Ver detalhes »</a></p>-->
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </section>

                                    <!--    PÁGINA PROJETOS-->
                                    <section id="pagProjetos"> 
                                        <div class="projetos">
                                            <div class="col-md-10 p-lg-5 mx-auto">      
                                                <div class="titulo-paginas texto-quemsomos">            
                                                    <span>Projetos</span>            
                                                </div>
                                                <div class="separador">                    
                                                    <img src="img/separador.png" alt="" style="width: 20%" />                    
                                                </div>

                                                <section>
                                                    <div class="container py-1">                    
                                                        <div class="row">
                                                            <div class="col-md-3 alignnone">
                                                                <img src="img/05.jpg" class="w-100">
                                                            </div> 
                                                            <div class="col-md-8 px-3">
                                                                <div class="card-block px-2 py-2">
                                                                    <h4 class="card-title titulo-materia" style="font-weight: bold;">cerimônia de abertura do MIMB - 2019</h4>
                                                                    <p class="card-text"><small class="text-muted">Atualizados 3 minutos atrás</small></p>
                                                                    <p class="card-text texto-eventos"> Evento realizado pelo SESC Pelourinho, para cerimônia de abertura do MIMB, com Margaret Menezes como convidada especial. </p>
                                                                    <a href="#pagEventos" onclick="Texto1();" class="card-link">Leia mais...</a>
                                                                    <script type="text/javascript">
                                                                        function Texto1() {
                                                                            swal({
                                                                                title: "CERIMÔNIA DE ABERTURA MIMB - 2019",
                                                                                text: 'A <span style="color:#7ABABA";> Osupa Productions </span> teve a honra de patrocinar o evento MIMB - 2019. <br> A cerimônia de abertura da MIMB 2019 teve muitas surpresas. Aconteceu no dia 14 de agosto, além das exibições de filmes nacionais e internacionais, o evento teve a mistura potente e dançante da banda Afrocidade.',
                                                                                confirmButtonText: 'Voltar',
                                                                                confirmButtonColor: '#7ababa',
                                                                                html: true
                                                                            });
                                                                        }
                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div>                    
                                                    </div>        
                                                </section>

                                                <section>
                                                    <div class="container py-3">                    
                                                        <div class="row">
                                                            <div class="col-md-3 alignnone">
                                                                <img src="img/06.jpg" class="w-100">
                                                            </div> 
                                                            <div class="col-md-8 px-3">
                                                                <div class="card-block px-2 py-2">
                                                                    <h4 class="card-title titulo-materia" style="font-weight: bold;">futuro digital conference</h4>
                                                                    <p class="card-text"><small class="text-muted">Atualizados 3 minutos atrás</small></p>
                                                                    <p class="card-text texto-eventos"> Festival de Cinema Latino-Americano de Nova York (NYLFF), o principal festival de cinema da cidade de Nova York, programas de competição e programas comunitários. </p>
                                                                    <a href="#pagEventos" onclick="Texto2();" class="card-link">Leia mais...</a>
                                                                    <script type="text/javascript">
                                                                        function Texto2() {
                                                                            swal({
                                                                                title: "FUTURO DIGITAL CONFERENCE!",
                                                                                text: 'A Festival de Cinema Latino-Americano de Nova York (NYLFF), o principal festival de cinema da cidade de Nova York. Desde sua fundação em 1999, o NYLFF produz experiências culturalmente relevantes e divertidas que constroem platéias para o cinema latino, apoiam a comunidade cinematográfica com desenvolvimento profissional e fomentam relacionamentos para talentos latinos. A programação inclui o principal festival de cinema da cidade de Nova York, programas de competição e programas comunitários. ',
                                                                                confirmButtonText: 'Voltar',
                                                                                confirmButtonColor: '#7ababa',
                                                                                html: true
                                                                            });
                                                                        }
                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div>                    
                                                    </div>        
                                                </section>

                                                <section>
                                                    <div class="container py-3">                    
                                                        <div class="row">
                                                            <div class="col-md-3 alignnone">
                                                                <img src="img/07.jpg" class="w-100">
                                                            </div> 
                                                            <div class="col-md-8 px-3">
                                                                <div class="card-block px-2 py-2">
                                                                    <h4 class="card-title titulo-materia" style="font-weight: bold;">circuito baiano de judô</h4>
                                                                    <p class="card-text"><small class="text-muted">Atualizados 1 minutos atrás</small></p>
                                                                    <p class="card-text texto-eventos"> Circuito Baiano de Judô vencido por nosso atleta Gabriel Matos. </p>
                                                                    <a href="#pagEventos" onclick="Texto3();" class="card-link">Leia mais...</a>
                                                                    <script type="text/javascript">
                                                                        function Texto3() {
                                                                            swal({
                                                                                title: "Gabriel Matos - Atleta de Judô",
                                                                                text: 'A <span style="color:#7ABABA";> Osupa Productions </span> parabeniza nosso atleta patrocinado Gabriel Matos pela medalha de <span style="color:#FFD700";> Ouro </span> conquistada no Circuito Baiano de Judô..',
                                                                                confirmButtonText: 'Voltar',
                                                                                confirmButtonColor: '#7ababa',
                                                                                html: true
                                                                            });
                                                                        }
                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div>                    
                                                    </div>        
                                                </section>

                                            </div>
                                        </div>
                                        </div>
                                    </section>

<!--                                        PÁGINA GALERIA
                                    <section id="pagGaleria">      
                                        <div class="galeria">
                                            <div class="col-md-10 p-lg-5 mx-auto">      
                                                <div class="titulo-paginas">            
                                                    <span>Galeria</span>            
                                                </div>
                                                <div class="separador">                    
                                                    <img src="img/separador.png" alt="" style="width: 20%" />                    
                                                </div>
                                                <div id="myModal" class="modal">
                <span class="close">&times;</span>
                <img class="modal-content" id="img01">
                <div id="caption"></div>
            </div>
                                                <div>
                                                    <div class="card-deck mr-auto">
                                                        <div class="Portfolio2 " onclick="img('img1')"><a href="#!"><img class="card-img" id="img1" src="img/galeria/galeria1.jpg" alt=""></a><div class="desc">Imagem 1</div></div>
                                                        <div class="Portfolio2 " onclick="img('img1')"><a href="#!"><img class="card-img" id="img1" src="img/galeria/galeria2.jpg" alt=""></a><div class="desc">Imagem 1</div></div>
                                                        <div class="Portfolio2 " onclick="img('img1')"><a href="#!"><img class="card-img" id="img1" src="img/galeria/galeria3.jpg" alt=""></a><div class="desc">Imagem 1</div></div>
                                                      </div>
                                                </div>
                                                
                                            </div>
                                    </section>-->

                                    <!--        Pagína Contato-->
                                    <section id="pagContato">        
                                        <div class="contato"> 
                                            <div class="col-md-10  p-lg-5 mx-auto">                
                                                <div class="container contact">                   
                                                    <div class="row" style="margin-top: 3%">
                                                        <div class="col-md-5">                            
                                                            <div style="margin-top: 10%; text-align: center; color: white;">
                                                                <h1 class="titulo-contato" style="margin-bottom: 5%">Siga-nos nas redes sociais.</h1>      
                                                                <a href="https://twitter.com/OsupaProductio1" target="_blank" onmouseover="document.NOME.src = 'img/twitter.png'" onmouseout="document.NOME.src = 'img/twitter2.png'"><img src="img/twitter.png" name="NOME" Border="0"></a>                                
                                                                <a href="https://instagram.com/osupaproduction" target="_blank" onmouseover="document.NOME1.src = 'img/instagram.png'" onmouseout="document.NOME1.src = 'img/instagram2.png'"><img src="img/instagram.png" name="NOME1" Border="0"></a>
                                                                <a href="https://www.youtube.com/channel/UCtU4mZs9M193QUElia8jPqg" target="_blank" onmouseover="document.NOME2.src = 'img/youtube.png'" onmouseout="document.NOME2.src = 'img/youtube2.png'"><img src="img/youtube.png" name="NOME2" Border="0"></a>  <br>        

                                                            </div>
                                                            <!--                            </div>-->
                                                        </div>
                                                        <div class="col-md-7">
                                                            <div class="contact-form">				
                                                                <div class="form-group">
                                                                    <div class="contact-info col-sm-10" style="color: #fff1ce;">
                                                                        <div class="container">
                                                                            <div class="row">
                                                                                <div class="col-2" style="padding-left: 0;">
                                                                                    <img src="https://image.ibb.co/kUASdV/contact-image.png" alt="image"/>
                                                                                </div>
                                                                                <div class=" col-10" style="text-align: center;">
                                                                                    <h1 class="titulo-contato">Entre em contato</h1>  
                                                                                    <h4>Gostaríamos de ouvir você</h4> 
                                                                                </div>                                                
                                                                            </div>
                                                                        </div>                                        
                                                                    </div>
                                                                </div>
                                                                <form method="POST" action="processa.php">

                                                                    <div class="col-sm-10 form-group" style="margin-top: 3%;">          
                                                                        <input type="text" name="nome" class="form-control" placeholder="Digite seu nome" name="lname">
                                                                    </div>


                                                                    <div class="col-sm-10 form-group">
                                                                        <input type="email" name="email" class="form-control" placeholder="Digite seu email" name="email">
                                                                    </div>


                                                                    <div class="col-sm-10 form-group">
                                                                        <textarea name="mensagem" class="form-control" rows="5" id="comment"></textarea>
                                                                    </div>


                                                                    <div class="col-sm-offset-2 col-sm-10 form-group">
                                                                        <button type="submit" class="btn btn-light" style=" float:right">Enviar</button>
                                                                    </div>                            

                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>           
                                        </div> 
                                    </section>   



                                    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                                    <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script></body>
                                    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
                                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                                    <script>
                                                                    // Get the modal
                                                                    var modal = document.getElementById("myModal");

                                                                    // Get the image and insert it inside the modal - use its "alt" text as a caption
                                                                    function img(imagem) {
                                                                        var img = document.getElementById(imagem);
                                                                        var modalImg = document.getElementById("img01");
                                                                        var captionText = document.getElementById("caption");
                                                                        img.onclick = function () {
                                                                            modal.style.display = "block";
                                                                            modalImg.src = this.src;
                                                                            captionText.innerHTML = this.alt;
                                                                        }
                                                                    }

                                                                    // Get the <span> element that closes the modal
                                                                    var span = document.getElementsByClassName("close")[0];

                                                                    // When the user clicks on <span> (x), close the modal
                                                                    span.onclick = function () {
                                                                        modal.style.display = "none";
                                                                    }
                                    </script>
                                    </body>
                                    </html>
